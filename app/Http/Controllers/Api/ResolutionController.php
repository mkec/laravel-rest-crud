<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreResolution;
use App\Http\Resources\Resolution as ResolutionResource;
use App\Http\Resources\ResolutionCollection;
//html template instead of markdown
use App\Mail\ResolutionCreated;
use App\Mail\ResolutionCreatedMarkdown;
use App\Resolution;
//without route request validation
use Illuminate\Database\Eloquent\ModelNotFoundException;
//without param validation custom request
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Request as FacadesRequest;

/**
 * Laravel Controller class - Resolution controller
 *
 * Class for CRUD manipulation over "resolution" resources
 *
 * PHP version 7.3
 *
 * @category Description
 * @package  Example
 * @author   Miloš Kecman <milos.kecman@gmail.com>
 * @license  MIT https://opensource.org/licenses/MIT
 * @version  GIT $id$
 * @access   public
 * @link     localhost
 */

class ResolutionController extends Controller
{
    /**
     * authentification handling
     */
    public function __construct()
    {
        $this->middleware('auth:api')->only(['store','update','destroy']);
    }

    /**
     * List resource collection with pagination
     *
     * @param Request $request
     * @return json collection resource
     */
    public function index(Request $request)
    {
        $per_page = $request->input('per_page') ?? 15;
        return new ResolutionCollection(
            Resolution::paginate($per_page)
                ->appends([
                    'per_page' => $per_page
                ])
        );
    }

    /**
     * Storing resource using custom request class with custom validation
     * Creating reminder email queue with markdown email template
     *
     * @param StoreResolution $request
     * @return json resource added with status code
     */
    public function store(StoreResolution $request)
    {
        $validatedData = $request->validated();
        $validatedData['user_id'] = $request->user()->id;

        $resolution = Resolution::create($validatedData);

        /* $resolution = Resolution::create([
            'title' => $request->title,
            'content' => $request->content,
            'reminder' => $request->reminder,
            'user_id' => $request->user_id,
        ]); */
        //immediately sent
        /* Mail::to($resolution->user)->send(
            new ResolutionCreatedMarkdown($resolution)
        ); */
        //sent first into queue
        /* Mail::to($resolution->user)->queue(
            new ResolutionCreatedMarkdown($resolution)
        ); */

        //mail sent after "reminder" number of days
        $when = $resolution->reminder==0 ? now() : now()->addDays($resolution->reminder);
        Mail::to($resolution->user)->later(
            $when,
            new ResolutionCreatedMarkdown($resolution)
        );
        return response()->json(new ResolutionResource($resolution), 201);
    }

    /**
     * Show specific resource
     * Here we are using custom request with custom validation
     * Request is also validating route param
     *
     * @param StoreResolution $request
     * @param integer $id
     * @return json specific resource item
     */
    public function show($id)
    {
        // show(int $id) -> validation issue if param is non numeric generate exception
        if (!is_numeric($id)) {
            throw new ModelNotFoundException();
        }
        $resolution = Resolution::findOrfail($id);
        return response()->json(new ResolutionResource($resolution));
    }

    /**
     * Updating specific resource
     *
     * @param StoreResolution $request
     * @param integer $id
     * @return json updated resource
     */
    public function update(StoreResolution $request, $id)
    {
        if (!is_numeric($id)) {
            throw new ModelNotFoundException();
        }

        $validatedData = $request->validated();
        $resolution = Resolution::findOrfail($id);

        $this->authorize($resolution);

        $resolution->update($validatedData);
        return response()->json(new ResolutionResource($resolution));
    }

    /**
     * Deleting specific resource
     *
     * @param integer $id
     * @return integer status code
     */
    public function destroy($id)
    {
        if (!is_numeric($id)) {
            throw new ModelNotFoundException();
        }
        $resolution = Resolution::findOrfail($id);

        $this->authorize($resolution);

        $resolution->delete();
        return response()->json(null, 204);
    }
}
