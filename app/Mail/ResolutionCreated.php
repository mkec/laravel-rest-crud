<?php

namespace App\Mail;

use App\Resolution;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ResolutionCreated extends Mailable
{
    use Queueable, SerializesModels;

    public $resolution;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Resolution $resolution)
    {
        $this->resolution = $resolution;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = "Resolution \"{$this->resolution->title}\" is successfully created";
        return $this
            ->subject($subject)
            ->view('emails.resolutions.created');
    }
}
