<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Resolution extends Model
{
    protected $guarded = ['id'];
    /**
     * ORM many to 1 relation
     *
     * @return void
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
