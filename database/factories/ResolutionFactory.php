<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Resolution;
use Faker\Generator as Faker;

$factory->define(Resolution::class, function (Faker $faker) {
    return [
        'title' => $faker->name,
        'content' => $faker->text,
        'reminder' => random_int(0, 99),
        'user_id' => 1
    ];
});
