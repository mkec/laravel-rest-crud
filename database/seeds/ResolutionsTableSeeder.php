<?php

use Illuminate\Database\Seeder;

class ResolutionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = App\User::all();
        $resolutionsCount = max((int) $this->command->ask('How many resolutions would you like?', 150), 10);
        factory(App\Resolution::class, $resolutionsCount)->make()->each(function ($resolution) use ($users) {
            $resolution->user_id = $users->random()->id;
            $resolution->save();
        });
    }
}
