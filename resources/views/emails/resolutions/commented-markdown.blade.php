@component('mail::message')
# Resolution created

Hi {{ $resolution->user->name }}

Resolution is created few minutesd ago.
Reminder for this resolution is set. You will be emailed every {{ $resolution->reminder }} days!
{{-- @component('mail::button', ['url' => ''])
Button Text
@endcomponent --}}

Thanks,<br>
{{ config('app.name') }}
@endcomponent
