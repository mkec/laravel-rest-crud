<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::namespace('Api')->group(function () {
    /* Route::post('/resolutions', 'ResolutionController@store');
    Route::get('/resolutions/{id}', 'ResolutionController@show');
    Route::put('/resolutions/{id}', 'ResolutionController@update');
    Route::delete('/resolutions/{id}', 'ResolutionController@destroy'); */
    Route::resource('/resolutions', 'ResolutionController', ['except' => 'edit']);

    Route::post('/register', 'Auth\RegisterController@register');
    Route::post('/login', 'Auth\LoginController@login');
    Route::post('/logout', 'Auth\LoginController@logout');
});

Route::fallback(function () {
    return response()->json([
        'message' => 'Resource not found',
    ], 404);
})->name('api.fallback');

Auth::routes();
