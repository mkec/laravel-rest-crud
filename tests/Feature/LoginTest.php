<?php

namespace Tests\Feature;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Log;
use Tests\TestCase;

class LoginTest extends TestCase
{
    use RefreshDatabase;

    public function testRequiresEmailAndLogin()
    {
        $this->json('POST', 'api/login')
            ->assertStatus(422)
            ->assertJson([
                'message' => 'The given data was invalid.',
                'errors' => [
                    'email' => ['The email field is required.'],
                    'password' => ['The password field is required.']
                ]
            ]);
    }


    public function testUserLoginsSuccessfully()
    {
        //create user for specific password
        factory(User::class)->create([
            'email' => 'login@user.com',
            'password' => bcrypt('password'),
        ]);
        //now when user exists we can try to login
        $user_data = ['email' => 'login@user.com', 'password' => 'password'];
        //try to login
        $this->json('POST', 'api/login', $user_data)
            ->assertStatus(200)
            ->assertJsonStructure([
                'data' => ['id','name','email','created_at','updated_at','api_token'],
            ]);
    }
}
