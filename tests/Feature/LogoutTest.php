<?php

namespace Tests\Feature;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class LogoutTest extends TestCase
{
    use RefreshDatabase;

    public function testUserIsLoggedOutProperly()
    {
        //login
        $user = factory(User::class)->create(['email' => 'login@user.com']);
        $token = $user->generateToken();
        $headers = ['Authorization' => "Bearer $token"];
        //get resolutions and logout
        $this->json('get', '/api/resolutions', [], $headers)->assertStatus(200);
        $this->json('post', '/api/logout', [], $headers)->assertStatus(200);
        //check if user is logedout
        $user = User::find($user->id);
        $this->assertEquals(null, $user->api_token);
    }

    public function testUserWithNullToken()
    {
        //login
        $user = factory(User::class)->create(['email' => 'login@user.com']);
        $token = $user->generateToken();
        $headers = ['Authorization' => "Bearer $token"];
        //logout
        $user->api_token = null;
        $user->save();
        //try to update
        $resolution = $this->create('Resolution');
        $response = $this->actingAs($this->user(), 'api')->json('PUT', "api/resolutions/$resolution->id", [
            'title' => $resolution->title . "_updated",
            'content' => $resolution->content . "_updated"
        ]);
        $response->assertStatus(403);
    }
}
