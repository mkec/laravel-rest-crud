<?php

namespace Tests\Feature;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Password;
use Tests\TestCase;

class PasswordResetTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Password Reset Request
     *
     * @return void
     */
    public function testsPasswordResetRequestedSuccessfully()
    {
        //create user
        factory(User::class)->create([
            'email' => 'login@user.com'
        ]);
        //prepare data for reset
        $user_data = [
            'email' => 'login@user.com',
        ];
        //try to reset and check if response is success
        $this->json('post', '/api/password/email', $user_data)
            ->assertStatus(201)
            ->assertJsonStructure(['message', 'status'])
            ->assertJson([
                'message' => 'Reset link sent to your email.',
                'status' => true
            ]);
    }

    /**
     * Password Reset wrong email
     *
     * @return void
     */
    public function testsPasswordResetWrongEmail()
    {
        //create user
        factory(User::class)->create([
            'email' => 'login@user.com'
        ]);
        //prepare data for reset
        $user_data = [
            'email' => 'logins@user.com',
        ];
        //try to reset and check if response is success
        $this->json('post', '/api/password/email', $user_data)
            ->assertStatus(401)
            ->assertJsonStructure(['message', 'status'])
            ->assertJson([
                'message' => 'Unable to send reset link',
                'status' => false
            ]);
    }

    /**
     * Password Reset Updated
     *
     * @return void
     */
    public function testsPasswordResetUpdateSuccessfully()
    {
        //login for specific email
        $user = factory(User::class)->create(['email' => 'login@user.com']);
        $reset_token = Password::broker()->createToken($user);

        $user_data = [
            "email" => "login@user.com",
            "token" => $reset_token,
            "password" => "password",
            "password_confirmation" => "password"
        ];
        //try to add new password and check if response is success
        $this->json('post', '/api/password/reset', $user_data)
            ->assertStatus(201)
            ->assertJsonStructure(['message', 'status'])
            ->assertJson([
                'message' => 'Password successfully changed.',
                'status' => true
            ]);
    }
}
