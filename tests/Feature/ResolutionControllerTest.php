<?php

namespace Tests\Feature;

use Faker\Factory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Log;
use Tests\TestCase;

class ResolutionControllerTest extends TestCase
{
    use RefreshDatabase;

    public function testCanReturnCollectionOfResources()
    {
        $resolution = $this->create('Resolution');
        $resolution2 = $this->create('Resolution');
        $resolution3 = $this->create('Resolution');

        $response = $this->actingAs($this->user(), 'api')
            ->json('GET', '/api/resolutions');
        $response
            ->assertJsonStructure([
                'data' => [
                    '*' => [
                        'id', 'title', 'content', 'reminder', 'created_at'
                    ]

                ],
                'links' => ['first', 'last', 'prev', 'next'],
                'meta' => [
                    'current_page', 'last_page', 'from', 'to', 'path', 'per_page', 'total'
                ]
            ])
            ->assertStatus(200);
    }
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testCanCreateResolution()
    {
        $faker = Factory::create();
        //Given
            // user is authenticated
        //When
            // post request create product
        $response = $this->actingAs($this->user(), 'api')->json('POST', '/api/resolutions', [
            'title' => $title = $faker->name,
            'content' => $content = $faker->text,
            'reminder' => $reminder = random_int(0, 99),
            'user_id' => $this->user()->id
        ]);
        //Log::info($response->getContent());
        //Then
            // product exists
        $response
            ->assertJsonStructure([
                'id', 'title', 'content', 'reminder', 'created_at'
            ])
            ->assertJson([
                'title' => $title,
                'content' => $content,
                'reminder' => $reminder
            ])
            ->assertStatus(201);
        $this->assertDatabaseHas('resolutions', [
            'title' => $title,
            'content' => $content,
            'reminder' => $reminder
        ]);
    }

    public function testWillFailWithA404IfResolutionIsNotFound()
    {
        $response = $this->json('GET', 'api/resolutions/-1');
        $response->assertStatus(404);
    }

    public function testCanReturnResolution()
    {
        //Given
        $resolution = $this->create('Resolution');
        //When
        $response = $this->json('GET', "api/resolutions/{$resolution->id}");
        //Then
        $response
            ->assertExactJson([
                'id' => $resolution->id,
                'title' => $resolution->title,
                'content' => $resolution->content,
                'reminder' => $resolution->reminder,
                'created_at' => (string) $resolution->created_at,
                //'updated_at' => (string) $resolution->updated_at
            ])
            ->assertStatus(200);
    }

    public function testWillFailWithA422IfResolutionWeWantToUpdateIsNotFound()
    {
        $response = $this->actingAs($this->user(), 'api')->json('PUT', 'api/resolutions/-1');
        $response->assertStatus(422);
    }

    public function testCanUpdateResolution()
    {
        $resolution = $this->create('Resolution');
        $response = $this->actingAs($this->user(), 'api')->json('PUT', "api/resolutions/$resolution->id", [
            'title' => $resolution->title . "_updated",
            'content' => $resolution->content . "_updated"
        ]);

        $response
            ->assertExactJson([
                'id' => $resolution->id,
                'title' => $resolution->title . "_updated",
                'content' => $resolution->content . "_updated",
                'reminder' => $resolution->reminder,
                'created_at' => (string) $resolution->created_at
            ])
            ->assertStatus(200);

        $this->assertDatabaseHas('resolutions', [
            'id' => $resolution->id,
            'title' => $resolution->title . "_updated",
            'content' => $resolution->content . "_updated",
            'reminder' => $resolution->reminder,
            'created_at' => (string) $resolution->created_at,
            'updated_at' => (string) $resolution->updated_at
        ]);
    }

    public function testWillFailWithA404IfResolutionWeWantToDeleteIsNotFound()
    {
        $response = $this->actingAs($this->user(), 'api')->json('DELETE', 'api/resolutions/-1');
        $response->assertStatus(404);
    }

    public function testCanDeleteResolution()
    {
        $resolution = $this->create('Resolution');
        $response = $this->actingAs($this->user(), 'api')->json('DELETE', "api/resolutions/$resolution->id");
        $response->assertStatus(204)
            ->assertSee(null);
        $this->assertDatabaseMissing('resolutions', [
            'id' => $resolution->id
        ]);
    }
}
