<?php

namespace Tests;

use App\Http\Resources\Resolution as ResolutionResource;
use App\User;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    protected function user()
    {
        return factory(User::class)->create();
    }

    public function create(string $model, array $attributes = [], $resource = true)
    {
        $resourceModel = factory("App\\$model")->create($attributes);
        $resourceClass = "App\\Http\\Resources\\$model";

        if (!$resource) {
            return $resourceModel;
        }

        return new $resourceClass($resourceModel);
    }
}
